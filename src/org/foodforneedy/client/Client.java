package org.foodforneedy.client;

import java.util.*;

import org.foodforneedy.exceptions.HotelNotFoundException;
import org.foodforneedy.exceptions.InvalidFoodTypeException;
import org.foodforneedy.exceptions.InvalidLocalityException;
import org.foodforneedy.model.Hotel;
import org.foodforneedy.model.Ngo;
import org.foodforneedy.service.HotelServiceImpl;
import org.foodforneedy.service.IHotel;
import org.foodforneedy.service.INgo;
import org.foodforneedy.service.NgoImpl;

public class Client {
	public static void main(String[] args) {
		IHotel ihotel = new HotelServiceImpl();
		INgo ingo = new NgoImpl();
		try (Scanner sc = new Scanner(System.in);) {
			System.out.println("Enter A for admin access or N for Ngo access");
			String input = sc.next();
			switch (input) {
			case "A":
				System.out.println("Enter username");
				String username = sc.next();
				if (username.equals("admin")) {
					System.out.println("Enter password");
					String password = sc.next();
					if (password.equals("admin")) {
						System.out.println("Welcome admin");
						System.out.println(
								"Enter 1 for adding hotels Enter 2 for removing hotels Enter 3 for adding food availability");
						int adminInput = sc.nextInt();
						switch (adminInput) {
						case 1:
							Hotel hotel = new Hotel();
							System.out.println("Enter hotel id");
							hotel.setHotelId(sc.nextInt());
							System.out.println("Enter hotel name");
							sc.nextLine();
							hotel.setHotelName(sc.nextLine());
							System.out.println("Enter foodtype");
							hotel.setFoodType(sc.next());
							System.out.println("Enter date string");
							hotel.setDate(sc.next());
							System.out.println("enter plates");
							hotel.setNoOfPlates(sc.nextInt());
							ihotel.addHotel(hotel);
							break;
						case 2:
							System.out.println("Enter id to remove hotel");
							int id = sc.nextInt();
							try {
								ihotel.removeHotel(id);
								System.out.println("hotel removed");
							} catch (Exception e1) {
								System.out.println(e1.getMessage());
							}
							break;
						case 3:
							System.out.println("Enter availability details id/food type/plates");
							int hotelId = sc.nextInt();
							String foodType = sc.next();
							int noOfPlates = sc.nextInt();
							try {
								ihotel.addFoodAvailability(hotelId, foodType, noOfPlates);
								System.out.println("Food availability successfully added");
							} catch (Exception e) {
								System.out.println(e.getMessage());
							}
						}
					} else {
						System.out.println("password dosen't match");
					}
				} else {
					System.out.println("Invalid username");
				}
				break;
			case "N":
				System.out.println("Enter 1 to login Enter 2 to register");
				int orgInput = sc.nextInt();
				switch (orgInput) {
				case 1:
					System.out.println("Enter id");
					int id = sc.nextInt();
					try {
						Ngo ngo = ingo.getNgosById(id);

						if (ngo != null) {
							System.out.println("Enter password");
							String password = sc.next();
							if (ngo.getPassword().equals(password)) {
								System.out.println("Welcome " + ngo.getOrgName());
								System.out.println(
										"Enter 1 to view all hotels 2 to get hotels by locality 3 to get hotels foodtype "
												+ "4 to get hotels by foodtype and locality");
								int userIn = sc.nextInt();
								sc.nextLine();
								switch (userIn) {
								case 1:
									System.out.println(ihotel.viewAllHotels());
									break;
								case 2:
									System.out.println("Enter locality");
									sc.nextLine();
									String locality = sc.nextLine();
									try {
										System.out.println(ihotel.getHotelsByLocality(locality));
									} catch (InvalidLocalityException e) {
										System.out.println(e.getMessage());
									}
									break;
								case 3:
									System.out.println("Enter foodType");
									sc.nextLine();
									String foodType = sc.nextLine();
									try {
										System.out.println(ihotel.getHotelsByFoodtype(foodType));
									} catch (InvalidFoodTypeException e) {
										System.out.println(e.getMessage());
									}
									break;
								case 4:
									System.out.println("Enter locality and foodtype");
									
									String locality1 = sc.nextLine();
									String foodType1 = sc.nextLine();
									try {
										System.out.println(ihotel.getHotelsByFoodtypeAndLocality(locality1, foodType1));
									} catch (HotelNotFoundException e) {
										System.out.println(e.getMessage());
									}
									break;
								}
							} else {
								System.out.println("password dosen't match");
							}
						}
					} catch (Exception e) {
						System.out.println(e.getMessage());
						e.printStackTrace();
					}
					break;

				case 2:
					Ngo ngo = new Ngo();
					System.out.println("enter ngo id");
					int ngoId = sc.nextInt();
					ngo.setId(ngoId);
					sc.nextLine();
					System.out.println("enter ngo name");
					String ngoName = sc.nextLine();
					ngo.setOrgName(ngoName);
					System.out.println("enter ngo password");
					String ngoPassword = sc.nextLine();
					ngo.setPassword(ngoPassword);
					System.out.println("enter ngo locality");
					String ngoLocality = sc.nextLine();
					ngo.setLocality(ngoLocality);
					ingo.addNgo(ngo);
					System.out.println("ngo registered successfully");
				}

			}
		}

	}
}
