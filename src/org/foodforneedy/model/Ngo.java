package org.foodforneedy.model;

public class Ngo {
	private String orgName;
	private int id;
	private String locality;
	private String password;

	public Ngo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Ngo [orgName=" + orgName + ", locality=" + locality + "]";
	}

}
