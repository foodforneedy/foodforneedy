package org.foodforneedy.model;


public class Hotel {
	private String hotelName;
	private String foodType;
	private String date;
	private int hotelId;
	private int noOfPlates;
	boolean availability;
	
	public boolean isAvailability() {
		return availability;
	}

	public void setAvailability(boolean availability) {
		this.availability = availability;
	}

	public Hotel() {
		super();
		// TODO Auto-generated constructor stub
	} 
	
	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getFoodType() {
		return foodType;
	}

	public void setFoodType(String foodType) {
		this.foodType = foodType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getHotelId() {
		return hotelId;
	}

	public void setHotelId(int hotelId) {
		this.hotelId = hotelId;
	}

	public int getNoOfPlates() {
		return noOfPlates;
	}


	public void setNoOfPlates(int noOfPlates) {
		this.noOfPlates = noOfPlates;
	}

	@Override
	public String toString() {
		return "Hotel [hotelName=" + hotelName + ", foodType=" + foodType + ", date=" + date + ", hotelId=" + hotelId
				+ ", noOfPlates=" + noOfPlates + "]";
	}

	
	
	

}
