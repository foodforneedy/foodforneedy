package org.foodforneedy.service;

import java.util.List;

import org.foodforneedy.dao.HotelImplRepo;
import org.foodforneedy.dao.IHotelRepo;
import org.foodforneedy.exceptions.HotelNotFoundException;
import org.foodforneedy.exceptions.InvalidHoteIdException;
import org.foodforneedy.exceptions.InvalidLocalityException;
import org.foodforneedy.model.Hotel;

public class HotelServiceImpl implements IHotel {
	IHotelRepo ihotelRepo = new HotelImplRepo();

	@Override
	public void addHotel(Hotel hotel) {
		ihotelRepo.addHotel(hotel);

	}

	@Override
	public void removeHotel(int hotelId) throws Exception{
		ihotelRepo.removeHotel(hotelId);
	}

	@Override
	public void addFoodAvailability(int hotelId, String foodType, int noOfPlates)
			throws InvalidHoteIdException, Exception {
		if (hotelId != 0) {
			ihotelRepo.addFoodAvailability(hotelId, foodType, noOfPlates);
		} else {
			throw new InvalidHoteIdException("hotel id not found");
		}

	}
	
	@Override
	public List<Hotel> viewAllHotels() throws Exception {
		List<Hotel> hotelsList = ihotelRepo.viewAllHotels();
		return hotelsList;
	}

	@Override
	public List<Hotel> getHotelsByLocality(String locality) throws InvalidLocalityException, Exception {
		List<Hotel> hotelsList = ihotelRepo.getHotelsByLocality(locality);
		if (hotelsList.size() == 0) {
			throw new InvalidLocalityException("Locality not found");
		}
		return hotelsList;
	}

	@Override
	public List<Hotel> getHotelsByFoodtype(String foodType) throws Exception {
		List<Hotel> hotelsList = ihotelRepo.getHotelsByFoodtype(foodType);
		if (hotelsList.size() == 0) {
			throw new InvalidLocalityException("Locality not found");
		}
		return hotelsList;
	}

	@Override
	public List<Hotel> getHotelsByFoodtypeAndLocality(String locality, String foodType)
			throws HotelNotFoundException, Exception {
		List<Hotel> hotelsList = ihotelRepo.getHotelsByFoodtypeAndLocality(locality, foodType);
		if (hotelsList.size() == 0) {
			throw new HotelNotFoundException("locality / food type not found");
		}
		return hotelsList;
	}


}
