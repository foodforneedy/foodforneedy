package org.foodforneedy.service;

import java.util.List;

import org.foodforneedy.model.Hotel;
import org.foodforneedy.model.Ngo;

public interface INgo {
	void addNgo(Ngo ngoObj);
	
	
	Ngo getNgosById(int id) throws Exception;
}
