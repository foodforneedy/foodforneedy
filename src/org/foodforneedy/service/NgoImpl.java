package org.foodforneedy.service;

import java.util.List;

import org.foodforneedy.dao.HotelImplRepo;
import org.foodforneedy.dao.IHotelRepo;
import org.foodforneedy.dao.INgoRepo;
import org.foodforneedy.dao.NgoRepoImpl;
import org.foodforneedy.exceptions.HotelNotFoundException;
import org.foodforneedy.exceptions.InvalidHoteIdException;
import org.foodforneedy.exceptions.InvalidLocalityException;
import org.foodforneedy.model.Hotel;
import org.foodforneedy.model.Ngo;

public class NgoImpl implements INgo {
	INgoRepo ingoRepo = new NgoRepoImpl();
	IHotelRepo ihotelRepo = new HotelImplRepo();

	@Override
	public void addNgo(Ngo ngoObj) {
		ingoRepo.addNgo(ngoObj);
	}

	@Override
	public Ngo getNgosById(int id) throws InvalidHoteIdException, Exception {

		Ngo ngo = ingoRepo.getNgosById(id);
		if (ngo == null) {
			throw new InvalidHoteIdException("id not found");
		}
		return ngo;
	}

	
}
