package org.foodforneedy.service;

import java.util.List;

import org.foodforneedy.exceptions.HotelNotFoundException;
import org.foodforneedy.exceptions.InvalidHoteIdException;
import org.foodforneedy.model.Hotel;

public interface IHotel {
	// by admin
		void addHotel(Hotel hotel);
		void removeHotel(int hotelId) throws HotelNotFoundException, Exception;
		void addFoodAvailability(int hotelId, String foodType, int noOfPlates) throws InvalidHoteIdException, Exception;
		 
		List<Hotel> viewAllHotels() throws Exception;
		List<Hotel> getHotelsByLocality(String locality) throws Exception;
		List<Hotel> getHotelsByFoodtype(String foodType) throws Exception;
		List<Hotel> getHotelsByFoodtypeAndLocality(String locality, String foodType) throws Exception;
}
