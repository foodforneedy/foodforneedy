package org.foodforneedy.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.foodforneedy.model.Ngo;

public class NgoRepoImpl implements INgoRepo {

	Connection connection;
	PreparedStatement ps;

	
	
	

	public Ngo getNgosById(int id) throws Exception {
		connection = ModelDao.openConnection();
		ps = null;
		Ngo ngo = null;
		try {
			String query = "select * from organization where id=?";
			ps = connection.prepareStatement(query);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ngo = new Ngo();
				ngo.setOrgName(rs.getString("org_name"));
				ngo.setId(rs.getInt("id"));
				ngo.setLocality(rs.getString("locality"));
				ngo.setPassword(rs.getString("password"));
			}
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (ps != null) {
					ps.close();
				}
			} catch (SQLException e) {
				System.out.println(e);
				e.printStackTrace();
			}
		}
		return ngo;
	}

	@Override
	public void addNgo(Ngo ngo) {
		connection = ModelDao.openConnection();
		ps = null;

		try {
			String sqlQuery = "insert into organization values(?,?,?,?);";
			ps = connection.prepareStatement(sqlQuery);
			ps.setInt(1, ngo.getId());
			ps.setString(2, ngo.getOrgName());
			ps.setString(3, ngo.getPassword());
			ps.setString(4, ngo.getLocality());
	
			ps.execute();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
	}

	

}
