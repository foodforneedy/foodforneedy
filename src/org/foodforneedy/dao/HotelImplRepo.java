package org.foodforneedy.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import org.foodforneedy.model.Hotel;

public class HotelImplRepo implements IHotelRepo {
	Connection connection;
	PreparedStatement ps;

	@Override
	public void addHotel(Hotel hotel) {
		connection = ModelDao.openConnection();
		ps = null;

		try {
			String sqlQuery = "insert into hotel values(?,?,?,?,?,?,?,?);";
			ps = connection.prepareStatement(sqlQuery);
			ps.setInt(1, hotel.getHotelId());
			ps.setString(2, hotel.getHotelName());
			ps.setString(3, "9876543210");
			ps.setString(4, hotel.getFoodType());
			ps.setInt(5, hotel.getNoOfPlates());
			ps.setString(6,hotel.getDate());
			ps.setInt(7, 1);
			ps.setBoolean(8, true);
			ps.execute();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

	}

	@Override
	public void removeHotel(int hotelId) throws Exception {
		connection = ModelDao.openConnection();
		try {
			String query="delete from hotel where hotel_id=?";
			ps=connection.prepareStatement(query);
			ps.setInt(1,hotelId);
			ps.execute();
		}
		catch(SQLException e)
		{
			System.out.println(e);
		}
		finally {
			try {
				if (connection != null)
					connection.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				System.out.println(e);

			}
		}
	}

	@Override
	public void addFoodAvailability(int hotelId, String foodType, int noOfPlates) throws Exception {
		connection = ModelDao.openConnection();
		ps = null;
		try {
			String query = "update hotel set food_type=?, plates=?, availability=? where hotel_id=?";
			ps = connection.prepareStatement(query);
			ps.setString(1, foodType);
			ps.setInt(2, noOfPlates);
			ps.setBoolean(3, true);
			ps.setInt(4, hotelId);
			ps.execute();
		} catch (SQLException e) {
			System.out.println(e);
			throw e;
		} finally {
			try {
				if (connection != null)
					connection.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				System.out.println(e);

			}
		}

	}
	@Override
	public List<Hotel> getHotelsByFoodtypeAndLocality(String locality, String foodType) throws Exception {
		connection = ModelDao.openConnection();
		ps = null;
		List<Hotel> hotelsList = new ArrayList<Hotel>();
		System.out.println(locality);
		System.out.println(foodType);
		try {
			String query = "select * from hotel natural join address where area=? and food_type=?";
			ps = connection.prepareStatement(query);
			ps.setString(1, locality);
			ps.setString(2, foodType);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Hotel hotel = new Hotel();
				hotel.setHotelName(rs.getString("hotel_name"));
				hotel.setHotelId(rs.getInt("hotel_id"));
				hotel.setFoodType(rs.getString("food_type"));
				hotel.setNoOfPlates(rs.getInt("plates"));
				hotel.setDate(rs.getString("date"));
				hotelsList.add(hotel);
			}
		} catch (SQLException e) {
			System.out.println(e);
			throw e;
		} finally {
			try {
				if (connection != null)
					connection.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				System.out.println(e);

			}
		}
		return hotelsList;
	}

	@Override
	public List<Hotel> viewAllHotels() throws Exception {
		connection = ModelDao.openConnection();
		ps = null;
		List<Hotel> hotelsList = new ArrayList<Hotel>();
		try {
			String query = "select * from hotel";
			ps = connection.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Hotel hotel = new Hotel();
				hotel.setHotelName(rs.getString("hotel_name"));
				hotel.setHotelId(rs.getInt("hotel_id"));
				hotel.setFoodType(rs.getString("food_type"));
				hotel.setNoOfPlates(rs.getInt("plates"));
				hotel.setDate(rs.getString("date"));
				hotelsList.add(hotel);
			}
		} catch (SQLException e) {
			System.out.println(e);
			throw e;
		} finally {
			try {
				if (connection != null)
					connection.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());

			}
		}
		return hotelsList;
	}
	@Override
	public List<Hotel> getHotelsByFoodtype(String foodType) throws Exception {
		connection = ModelDao.openConnection();
		ps = null;
		List<Hotel> hotelsList = new ArrayList<>();

		try {
			String query = "SELECT * FROM hotel where food_type= ?";
			ps = connection.prepareStatement(query);
			ps.setString(1, foodType);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Hotel hotel = new Hotel();
				hotel.setHotelName(rs.getString("hotel_name"));
				hotel.setHotelId(rs.getInt("hotel_id"));
				hotel.setFoodType(rs.getString("food_type"));
				hotel.setNoOfPlates(rs.getInt("plates"));
				hotel.setDate(rs.getString("date"));
				hotel.setAvailability(rs.getBoolean("availability"));
				hotelsList.add(hotel);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw e;
		} finally {
			try {
				if (connection != null)
					connection.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		
		// TODO Auto-generated method stub
		return hotelsList;
	}

	@Override
	public List<Hotel> getHotelsByLocality(String locality) throws Exception {
		connection = ModelDao.openConnection();
		ps = null;
		List<Hotel> hotelsByLocationList = new ArrayList<Hotel>();

		try {
			String query = "SELECT * FROM hotel NATURAL JOIN address WHERE area = ?";
			ps = connection.prepareStatement(query);
			ps.setString(1, locality);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Hotel hotel = new Hotel();
				hotel.setHotelName(rs.getString("hotel_name"));
				hotel.setHotelId(rs.getInt("hotel_id"));
				hotel.setFoodType(rs.getString("food_type"));
				hotel.setNoOfPlates(rs.getInt("plates"));
				hotel.setDate(rs.getString("date"));
				hotelsByLocationList.add(hotel);
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw e;
		} finally {
			try {
				if (connection != null)
					connection.close();
				if (ps != null)
					ps.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return hotelsByLocationList;
	}

	

}
