package org.foodforneedy.dao;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class CreateTable {

	public static void main(String[] args) {

		Properties properties = new Properties();

		try {
			properties.load(new FileReader("foodforneedy.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String url = (String) properties.get("driver");
		String username = (String) properties.get("username");
		String password = (String) properties.get("password");
		String sql = "CREATE TABLE organization (id int NOT NULL AUTO_INCREMENT, org_name varchar(25), password varchar(25), locality varchar(25), PRIMARY KEY (id));"; // copy SQL queries here

		Connection connection = null;
		PreparedStatement statement = null;
		try {
			connection = DriverManager.getConnection(url, username, password);
			statement = connection.prepareStatement(sql);
			statement.execute();
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (statement != null) {
					statement.close();
				}
			} catch (SQLException e) {
				System.out.println(e);
			}
		}
	}

}
