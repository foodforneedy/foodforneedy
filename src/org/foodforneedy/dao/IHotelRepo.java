package org.foodforneedy.dao;

import java.util.List;

import org.foodforneedy.model.Hotel;
import org.foodforneedy.model.Ngo;

public interface IHotelRepo {
	// by admin
	void addHotel(Hotel hotel);
	void removeHotel(int hotelId) throws Exception;
	void addFoodAvailability(int hotelId, String foodType, int noOfPlates) throws Exception;
	
	List<Hotel> viewAllHotels() throws Exception;
	List<Hotel> getHotelsByLocality(String locality) throws Exception;
	List<Hotel> getHotelsByFoodtype(String foodType) throws Exception;
	List<Hotel> getHotelsByFoodtypeAndLocality(String locality, String foodType) throws Exception;
	
	
	
}
