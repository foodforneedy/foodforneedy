package org.foodforneedy.dao;

import java.util.List;

import org.foodforneedy.model.Hotel;
import org.foodforneedy.model.Ngo;

public interface INgoRepo {
	
	void addNgo(Ngo ngoObj);
	
	Ngo getNgosById(int id) throws Exception;
}
