package org.foodforneedy.exceptions;

public class InvalidInputDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidInputDataException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidInputDataException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
